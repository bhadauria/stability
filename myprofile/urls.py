from django.conf.urls import include, url
# from django.contrib import admin
from myprofile.views import MyView
from rest_framework.routers import DefaultRouter
from myprofile import views

router = DefaultRouter()
router.register(r"account", views.MyView,base_name="acc")
urlpatterns = [
    url(r'^', include(router.urls)),
    ]