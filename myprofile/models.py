# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from account.models import *
# Create your models here.
class Service(models.Model):
	name = models.CharField(max_length=100,null=True,blank=True)

	def __str_(self):
		return self.name
class Designation(models.Model):
	name = models.CharField(max_length=100,null=True,blank=True)

	def __str_(self):
		return self.name
class MyProfile(models.Model):
	user = models.ForeignKey(MyUser,null=True,blank=True)	
	service = models.ForeignKey(Service,null=True,blank=True)			
	Designation = models.ForeignKey(Designation,null=True,blank=True)
	is_active = models.BooleanField(default=False)

	def __str_(self):
		return self.user.email	
