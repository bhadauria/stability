from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from account.models import *
from oauth2_provider.models import AccessToken, RefreshToken
from oauth2_provider.settings import oauth2_settings
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from django.contrib.auth import authenticate
from django.core.mail import send_mail
from oauth2_provider.models import AccessToken, Application, RefreshToken
from rest_framework.authentication import BasicAuthentication
import smtplib
from email.mime.multipart import MIMEMultipart
from email.MIMEText import MIMEText
from oauthlib.common import generate_token
import traceback
from myprofile.models import *
from django.utils.timezone import now, timedelta
from myproject import settings
from datetime import datetime
from rest_framework.renderers import TemplateHTMLRenderer
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect	
class SignUp(viewsets.ViewSet):
	"""
		singnup 
	"""
	def create(self, request):
		try:
			print ("try inside signup")
			print ("request-->",request.data)
			data = request.data
			# domain = data['email'].rstrip('@',1)
			try:
				instance = MyUser.objects.create(first_name=data['first_name'],last_name=data['last_name'],email=data['email'],mobile=data['mobile'])
			except Exception as e:
				print (e)
				if "mobile" in str(e):
					return Response({"is_mobile_exist":True},status=status.HTTP_404_NOT_FOUND)
				elif "email" in str(e):
					return Response({"is_email_exist":True},status=status.HTTP_404_NOT_FOUND)
			instance.set_password(request.data["password"])
			instance.save()            
			Application.objects.create(user=instance,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)          
			subject = 'email_verification'
			# message = "http://127.0.0.1:8000/verify/"
			message= "\r\n".join([
			# "From: xyz@gmail.com",
			# "To: @gmail.com",
			"Subject: Email Verification",
			"Click here",
			"http://127.0.0.1:8000/api/user/signup/"+str(instance.pk)+"/"
			])
			to=instance.email
			print "hhhhhh",to
			s = smtplib.SMTP('smtp.gmail.com:587')
			s.ehlo()
			s.starttls()
			s.login(settings.from_mail,settings.passs)
			s.sendmail(settings.from_mail, to, message)
			s.close()
			# send_mail(subject, message, "",to, fail_silently=False) 
			return Response(200)
		except Exception as e:
			print ('lllllllllllllll',e)
			instance.delete()
			return Response(status=status.HTTP_400_BAD_REQUEST)
			
	def retrieve(self,request,pk):
		try:
			obj = MyUser.objects.get(pk=pk)	
			obj.is_active=True
			obj.is_admin=True
			obj.save()
			name= obj.email.rsplit('@',1)[-1]
			sr = Service.objects.get_or_create(name=name)
			if MyProfile.objects.filter(service=sr[0],is_active=True).count()>=2:
				return Response({"key":"2 admin already registered"})	
			ds1=Designation.objects.filter(name__icontains='admin')
			ds = None
			for i in ds1:
				ds2 = MyProfile.objects.filter(Designation=i)
				if ds2.count()>1:
					pass
				else:
					ds = i			
			my = MyProfile.objects.create(user=obj,service=sr[0],Designation=ds,is_active=True)
			return Response('done')
		except Exception:
			traceback.print_exc()	
			return Response("Exception")
class Login(APIView):
	# authentication_class = (BasicAuthentication,)
	renderer_classes = [TemplateHTMLRenderer]
	def post(self,request):
		data=request.data
		print data['user'],data['pass']
		a=authenticate(username=data['user'],password=data['pass'])
		print a
		if a is not None and a.is_admin:
			token = get_token(a)
			return Response({"token":token},template_name = "account/add_user.html")
		else:
			token = get_token(request.user)			
			return Response(template_name = "account/index.html")
def get_token(user):
	try:	
		token = generate_token()
		refresh_token1 = generate_token()
		info = {"name":user.first_name,"email":user.email,"mobile":user.mobile,"status":user.is_active,"creation_date":user.created_at}
		app = Application.objects.get(user=user)
		print ("---------token-------")
		expire_time = oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS
		expires = now()+timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)
		scope = "read write"
		acces_token = AccessToken.objects.create(user = user,application = app,expires = expires,token = token,scope = scope)
		RefreshToken.objects.create(user = user,application = app,token = refresh_token1,access_token = acces_token)
		token = {
			"access_token" : acces_token.token,
			"expires_in" : expire_time,
			"refresh_token" : acces_token.refresh_token.token,
			"scope" : acces_token.scope,
			"client_id":app.client_id,
			"client_secret":app.client_secret
		}
		return ({"token":token,"info":info})		
	except Exception:
		traceback.print_exc()
		return 400			
class UserSignUp(APIView):
	"""
		singnup 
	"""
	permission_classes=(TokenHasReadWriteScope,)
	def post(self,request):
		try:
			print ("try inside signup",request.META)
			data1=dict(request.data.iterlists())
			print ("request-->",data1,request.user)
			designation = data1['designation']
			email= data1['email']
			ac_status=data1['ac_status']
			pr_status=data1['pr_status']
			date=data1['date']
			name=data1['name']
			org=MyProfile.objects.get(user=request.user).service
			print designation,"<<<",email,"<<<<",name,"<<<<",ac_status,"<<<",pr_status,"<<<<",date
			for em,des,ac,pr,dt,nm in zip(email,designation,ac_status,pr_status,date,name):
				# print (">>",em,des,ac,pr,dt,nm,datetime.strptime(dt,'%d-%m-%Y').strftime('%Y-%m-%d'))
				print MyProfile.objects.filter(service=org).count(),org
				if MyProfile.objects.filter(service=org).count()>3:
					return Response({"result":400},status=400)
				if em =='':
					continue	
				ds,fl  = Designation.objects.get_or_create(name=des)
				user,active = MyUser.objects.get_or_create(email=em)
				user.is_active=str(ac).title()
				name=nm.split(',')
				if len(name)==2:
					user.first_name,user.last_name=nm.split(',')
				else:
					user.first_name=name[0]	
				user.date=datetime.strptime(dt,'%d-%m-%Y').strftime('%Y-%m-%d')
				user.save()
				# if MyProfile.objects.filter(Designation=ds,is_active=True).exists():
					# return Response({"designation":"already exists"})
				myprofile,active=MyProfile.objects.get_or_create(user=user)
				myprofile.Designation=ds
				myprofile.is_active=str(pr).title()
				myprofile.service=org
				myprofile.save()	

			# send_mail(subject, message, "",email, fail_silently=False) 
			print "here"
			# return HttpResponseRedirect("api/user/show")
			return Response({"result":201,'service':org.pk})
		except Exception:
			traceback.print_exc()
			return Response({"result":	500})
	
	def retrieve(self,request,pk):
		try:
			obj = MyProfile.objects.get(pk=pk)	
			obj.is_active=True
			obj.user.is_active=True
			obj.user.save()
			obj.save()
			return Response('done')
		except Exception:
			traceback.print_exc()	
			return Response("Exception")
def login(request):
	return render(request,'account/index.html')	
class Show(APIView):
	renderer_classes = [TemplateHTMLRenderer]				
	def get(self,request):
		try:
			key=request.GET.get('key')
			print(">>>>>",key)
			obj=MyProfile.objects.filter(service__pk=key)
			print (obj)
			data=[]
			for i in obj:
				if i.is_active:
					is_active='Active'
				else:
					is_active='Not Active'	
				data.append({"designation":i.Designation.name,"name":i.user.first_name,"email":i.user.email,"date":i.user.date,"is_active":is_active})
			return Response({"data":data},template_name='account/show_user.html')						
		except Exception as e:
			traceback.print_exc()
			return Response(status=400,template_name='account/show_user.html')	