from django.conf.urls import include, url
# from django.contrib import admin
from account.views import SignUp
from rest_framework.routers import DefaultRouter
from account import views

router = DefaultRouter()
router.register(r"signup", views.SignUp,base_name="signup")
# router.register(r"usersignup", views.UserSignUp,base_name="usersignup")
# router.register(r"login",views.Login,base_name="login")
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^index',views.login),
    url(r'show',views.Show.as_view(),name="show"),
    url(r'login/$',views.Login.as_view(),name="login"),
    url(r'adduser/$',views.UserSignUp.as_view(),name="adduser")
    ]