from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import (BaseUserManager,AbstractBaseUser)
class MyUserManager(BaseUserManager):
    def create_user(self, email,mobile, password=None,**kwargs):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError("Users must have an email address")

        if not mobile:
            raise ValueError("Users must have a valid mobile number.")
        print ("sgdfhdfhdfg----->",mobile )           
        user = self.model(
            email=self.normalize_email(email), mobile= mobile
            
        )
        # user.is_staff = True
        user.set_password(password)
        user.save()
        # Application.objects.create(user=user, client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)

        return user

    def create_superuser(self, email, password,mobile, **kwargs):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        print ("sgdfhdfhdfg----->", mobile)
        user = self.create_user(email,
            password=password, mobile=mobile,**kwargs
            
        )
        user.is_admin = True
        # user.is_staff =True
        user.is_superuser=True
        user.is_active= True
        user.save()
        return user

# class Contact_No(models.Model):
#     number = models.CharField(max_length=50,blank=True,null=True)

#     def __unicode__(self):
#         return unicode(self.number)

class MyUser(AbstractBaseUser):
    """
    **MyUser Model** this contains every user registered with us. 
    this model is inherited by Student, Employee, Guardian.
    """

    email = models.EmailField(
        verbose_name="email address",
        max_length=255,
       unique=True,
    )    
    first_name = models.CharField(max_length=200,blank=True,null=True)
    last_name = models.CharField(max_length=200,blank=True,null=True)
    mobile = models.CharField(max_length=20)
    key=models.CharField(max_length = 128,blank=True,null=True)
    facebook_id = models.CharField(max_length=255,blank=True,null=True)
    is_admin = models.BooleanField(default=False)
    is_staff= models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    date = models.DateField(blank=True,null=True)
    objects = MyUserManager()

    USERNAME_FIELD = "email"
    # REQUIRED_FIELDS = ["email"]

    # def get_full_name(self):
    #     # The user is identified by their email address
    #     return self.email

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin    

