# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-11 12:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_myuser_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='mobile',
            field=models.CharField(max_length=20),
        ),
    ]
