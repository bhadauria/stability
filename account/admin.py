from django.contrib import admin
from account.models import MyUser,MyUserManager
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class MyUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    password = ReadOnlyPasswordHashField()
    list_display = ('email', 'is_admin','is_active')
    list_filter = ('is_admin','is_active')
  
#displayed fields
    fieldsets = (
        (None, {'fields': ('email', 'password','mobile','facebook_id','first_name','key')}),
        
        ('Permissions', {'fields': ('is_admin','is_active')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2','mobile')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
admin.site.register(MyUser,MyUserAdmin)
admin.site.unregister(Group)